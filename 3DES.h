#define ENCRYPTION_MODE 1
#define DECRYPTION_MODE 0
#define BLOCK_SIZE 64
#define NUMBER_OF_ROUNDS 16
#define ENCRYPTION_OUTPUT_FILE "encryption_output.txt"
#define DECRYPTION_OUTPUT_FILE "decryption_output.txt"

typedef struct {
    char c[29];
    char d[29];
    char sub_key[49];
} sub_key_set;

/* Enum for encryption or decryption */
typedef enum{
    encryption,
    decryption
}operation_mode;

typedef enum{
    true = 1,
    false = 0
} bool;

void shift_left(char *str_shifted, char *arr, int n);
char *substr(char *str, int start_index, int end_index);
char *string_to_binary(char *message);
char *read_64_bit_data_from_file(FILE *file, size_t *number_of_chars_read);
char *read_key_from_file(FILE *file);
void generate_key(FILE *file);
void clear_contents(FILE **output_file, operation_mode mode);
void write_to_file(FILE **output_file, char *encrypted_readable_message);
char *pkcs5_padding(char *message);
char *remove_padding_if_needed(char *message);
void encrypt(char *encrypted_message, char *plain_text_message, sub_key_set *sub_keys);
void decrypt(char *plain_text_message, char *encrypted_message, sub_key_set *sub_keys);
void generate_sub_keys(char *key, sub_key_set *sub_keys);
char *XOR(char *A, char *B);
int binary_to_decimal(char *binary);
char *decimal_to_binary(int decimal);
char *binary_to_string(char *encrypted_message);