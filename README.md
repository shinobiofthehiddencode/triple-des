﻿

## This is an implementation of the Triple Data Encryption Standard (3DES).


> Triple DES is a symmetric-key block cipher, which applies the DES algorithm thrice to each data block. DES takes a fixed-length string of plaintext bits and transforms it through a series of complicated operations into another ciphertext bitstring of the same length. In the case of DES, the block size is 64 bits. DES also uses a key to customize the transformation, so that decryption can supposedly only be performed by those who know the particular key used to encrypt. The key ostensibly consists of 64 bits; however, only 56 of these are actually used by the algorithm.

The current implementation assumes all the keys to be different but works just as fine with other keying options. PKCS5Padding is used as the padding scheme. Password-Based Key Derivation Function 2 (pbkdf2) has been used in the key generation algorithm. The passphrase in pbkdf2 has been salted using pseudorandom numbers taken from `/dev/urandom`


**Prerequisites:**

- A C compiler (Tested on gcc version 7.2.0)
- OS supported - Linux, MacOS

**Run the program:**

1. Compile the program: `gcc -Wall -Wextra run.c 3DES.c utilities.c -o 3DES -lm -lcrypto`

2. Generate the key using: `./3DES -g 'key_file_name'`, or create your own file containing the key.

3. For encryption, run the executable with the following syntax: 
            
            `./3DES -k 'k1.key k2.key k3.key' -m 'path_to_message_file' --encrypt`.

    For decryption, run:

            `./3DES -k "k1.key k2.key k3.key" -m 'path_to_encrypted_message_file' --decrypt`

    Assuming, k1.key, k2.key and k3.key store the keys required for the algorithm.

    *Note: It is mandatory to supply the option argument in the same order as shown above. Also, the key used for encryption and decryption must be the same and must be provided in the same order in both cases.*

**Issues:**

- Use unsigned char instead of string for processing the bits in 3DES. The current implementation is only a simulation of the algorithm.

- Generate a complete technical documentation of the project. (tools like Doxygen may be used.)

**References:**

- https://en.wikipedia.org/wiki/Triple_DES
- http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm
- https://stackoverflow.com/

**Author**

- Saksham Raj
